--DATABASE
create database ecommerce
go
use ecommerce;
go


create table Roles(
	
	RoleID int not null primary key identity(1,1),
	RoleName nvarchar(20) not null,

);
go

create table Category(
	
	CategoryID int not null primary key identity(1,1),
	CategoryName nvarchar(50) not null

);
go
--TABLES
Create Table [Users](

	UserID INT Primary Key Identity(1,1) Not null,
	RoleID int not null foreign key references Roles(RoleID),
	Username nvarchar(20) not null,
	[Password] nvarchar(15) not null,
	Email nvarchar(50) not null,
	Phone nvarchar(10) not null,
	Firstname nvarchar(30) not null,
	Lastname nvarchar(30) not null,
	CreatedAt Datetime2 not null default(GETDATE()),
	ModifiedAt Datetime2

);
go

Create Table [Address](
	
	AddressID int not null primary key identity(1,1),
	UserID int not null foreign Key References [Users](UserID),
	City nvarchar(30) not null,
	Country nvarchar(30) not null,
	[State] nvarchar(30) not null,
	PostalCode nvarchar(7) not null,
	AddressDesc nvarchar(MAX) not null

);
go

Create Table [Product](
	
	ProductID INT primary key Identity(1,1) not null,
	CategoryID int not null foreign key references Category(CategoryID),
	ProductBrand nvarchar(30) not null,
	ProductName nvarchar(30) not null,
	ProductDesc nvarchar(MAX),
	ProductPrice decimal(19,2) not null,
	ProductColor nvarchar(20),
	
);
go

create table Images(
	
	ImageID int not null primary key identity(1,1),
	ProductID int foreign key references Product(ProductID),
	ImageCaption nvarchar(50) not null,
	ImagePath nvarchar(max) not null

);
go

Create Table [Order](
	
	OrderID int primary key not null identity(1,1),
	UserID int not null foreign key references Users(UserID),
	AddressID int foreign key references [Address](AddressID),
	Total decimal(19,2) not null,
	OrderDate datetime2 not null default(GETDATE())

);
go

Create Table [OrderDetail](

	OrderDetailID int primary key not null identity(1,1),
	ProductID int foreign key references Product(ProductID),
	OrderID int foreign key references [Order](OrderID),
	Quantity int not null,
	UnitPrice decimal(19,2) not null

);
go

Create Table [Cart](
	RecordID int not null Primary key identity(1,1),
	CartID   nvarchar(max) not null,
	ProductID int not null foreign key references Product(ProductID),
	[Count] int not null,
	CreatedAt Datetime2 not null default(GETDATE()) 
);
go
--Alters

--INSERTS

--- Roles
INSERT INTO Roles (RoleName) values (N'Admin');
go
INSERT INTO Roles (RoleName) values (N'User');
go
---Users
INSERT INTO dbo.Users (RoleID,Username,[Password],Email,Phone,Firstname,Lastname)
Values (1,N'admin',N'12332112',N'admin@gmail.com',N'5435678980',N'Onur',N'�anl�');
go
INSERT INTO dbo.Users (RoleID,Username,[Password],Email,Phone,Firstname,Lastname) 
Values (2,N'o.sanli',N'12332112',N'o.sanli@gmail.com',N'5435678980',N'Onur',N'�anl�');
go
INSERT INTO dbo.Users (RoleID,Username,[Password],Email,Phone,Firstname,Lastname) 
Values (2,N'i.cinar',N'12332112',N'i.cinar@gmail.com',N'5537658909',N'�pek',N'��nar');
go

---Addresses
INSERT INTO dbo.Address (UserID,City,Country,[State],PostalCode,AddressDesc)
Values (1,N'�stanbul','Turkey','Turkey','34180','Ye�ilk�y');
go
INSERT INTO dbo.Address (UserID,City,Country,[State],PostalCode,AddressDesc)
Values (2,N'�stanbul','Turkey','Turkey','34170','Bak�rk�y');
go
INSERT INTO dbo.Address (UserID,City,Country,[State],PostalCode,AddressDesc)
Values (3,N'�stanbul','Turkey','Turkey','34170','Bak�rk�y');
go
---Category
INSERT INTO Category (CategoryName) Values (N'Telefon');
go
INSERT INTO Category (CategoryName) Values (N'Televizyon');
go
INSERT INTO Category (CategoryName) Values (N'Tablet');
go
INSERT INTO Category (CategoryName) Values (N'Masa�st� Bilgisayar');
go
INSERT INTO Category (CategoryName) Values (N'Bilgisayar Bile�enleri');
go
INSERT INTO Category (CategoryName) Values (N'Telefon Aksesuarlar�');
go

---Products

INSERT INTO [dbo].[Product] (CategoryID,ProductBrand,ProductName,ProductDesc,ProductPrice,ProductColor)
Values (1,N'One Plus',N'One Plus One',N'OneAndroid Lollipop with CyanogenOS 12 and OxygenOS - 2.5GHz Qualcomm Snapdragon 801 quad core processor, 578MHz Adreno-330 GPU, 3GB RAM, 64GB internal memory and single Micro SIM ', 1000.00,N'Black');
go
INSERT INTO [dbo].[Product] (CategoryID,ProductBrand,ProductName,ProductDesc,ProductPrice,ProductColor)
Values (1,N'One Plus',N'One Plus One',N'Android Lollipop with CyanogenOS 12 and OxygenOS - 2.5GHz Qualcomm Snapdragon 801 quad core processor, 578MHz Adreno-330 GPU, 3GB RAM, 64GB internal memory and single Micro SIM ', 1000.00,N'White');
go
INSERT INTO [dbo].[Product] (CategoryID,ProductBrand,ProductName,ProductDesc,ProductPrice,ProductColor)
Values (1,N'OnePlus',N'One Plus 3',N'OnePlus 3, RAM 6GB+ROM 64GB 4G FDD-LTE 5.5 inch Android 6.0 Smart Phone Qualcomm Snapdragon 820 Quad Core 2x2.2GHz + 2x1.6GHz, 8.0MP+16.0MP (Gold)', 1500.00,N'Gold' );
go
INSERT INTO [dbo].[Product] (CategoryID,ProductBrand,ProductName,ProductDesc,ProductPrice,ProductColor)
Values (1,N'One Plus',N'One Plus 3',N'OnePlus 3, RAM 6GB+ROM 64GB 4G FDD-LTE 5.5 inch Android 6.0 Smart Phone Qualcomm Snapdragon 820 Quad Core 2x2.2GHz + 2x1.6GHz, 8.0MP+16.0MP (Gold)', 1500.00,N'Black' );
go
INSERT INTO [dbo].[Product] (CategoryID,ProductBrand,ProductName,ProductDesc,ProductPrice,ProductColor)
Values (1,N'One Plus',N'One Plus 3',N'OnePlus 3, RAM 6GB+ROM 64GB 4G FDD-LTE 5.5 inch Android 6.0 Smart Phone Qualcomm Snapdragon 820 Quad Core 2x2.2GHz + 2x1.6GHz, 8.0MP+16.0MP (Gold)', 1500.00,N'White' );
go
INSERT INTO [dbo].[Product] (CategoryID,ProductBrand,ProductName,ProductDesc,ProductPrice,ProductColor)
Values (1,N'One Plus',N'One Plus 3t',N'OnePlus 3T 6GB- RAM 64GB-ROM A3000 5.5" Factory Unlocked Phone - 64 GB US Version- Gunmetal', 2000.00,N'Gunmetal' );
go
INSERT INTO [dbo].[Product] (CategoryID,ProductBrand,ProductName,ProductDesc,ProductPrice,ProductColor)
Values (1,N'One Plus',N'One Plus 3t',N'OnePlus 3T 6GB- RAM 64GB-ROM A3000 5.5" Factory Unlocked Phone - 64 GB US Version- Gunmetal', 2000.00,N'Gold' );
go
INSERT INTO [dbo].[Product] (CategoryID,ProductBrand,ProductName,ProductDesc,ProductPrice,ProductColor)
Values (1,N'One Plus',N'One Plus 3t',N'OnePlus 3T 6GB- RAM 64GB-ROM A3000 5.5" Factory Unlocked Phone - 64 GB US Version- Gunmetal', 2000.00,N'White' );
go
INSERT INTO [dbo].[Product] (CategoryID,ProductBrand,ProductName,ProductDesc,ProductPrice,ProductColor)
Values (1,N'One Plus',N'One Plus 5',N'OnePlus 5 A5000 8GB RAM / 128GB Midnight Black Factory Unlocked USA Version', 2500.00,N'Midnight Black' );
go
INSERT INTO [dbo].[Product] (CategoryID,ProductBrand,ProductName,ProductDesc,ProductPrice,ProductColor)
Values (1,N'One Plus',N'One Plus 5',N'OnePlus 5 A5000 8GB RAM / 128GB Midnight Black Factory Unlocked USA Version', 2500.00,N'Midnight Blue' );
go
INSERT INTO [dbo].[Product] (CategoryID,ProductBrand,ProductName,ProductDesc,ProductPrice,ProductColor)
Values (1,N'One Plus',N'One Plus 5',N'OnePlus 5 A5000 8GB RAM / 128GB Midnight Black Factory Unlocked USA Version', 2500.00,N'Gold' );
go
INSERT INTO [dbo].[Product] (CategoryID,ProductBrand,ProductName,ProductDesc,ProductPrice,ProductColor)
Values (1,N'One Plus',N'One Plus 5',N'OnePlus 5 A5000 8GB RAM / 128GB Midnight Black Factory Unlocked USA Version', 2500.00,N'Black' );
go
INSERT INTO [dbo].[Product] (CategoryID,ProductBrand,ProductName,ProductDesc,ProductPrice,ProductColor)
Values (1,N'One Plus',N'One Plus 5t',N'OnePlus 5T A5010 64GB Midnight Black, Dual Sim, 6.01", 6GB RAM, GSM Unlocked International Model, No Warranty', 3000.00,N'Midnight Black' );
go
INSERT INTO [dbo].[Product] (CategoryID,ProductBrand,ProductName,ProductDesc,ProductPrice,ProductColor)
Values (1,N'One Plus',N'One Plus 5t',N'OnePlus 5T A5010 64GB Midnight Black, Dual Sim, 6.01", 6GB RAM, GSM Unlocked International Model, No Warranty', 3000.00,N'gold' );
go
INSERT INTO [dbo].[Product] (CategoryID,ProductBrand,ProductName,ProductDesc,ProductPrice,ProductColor)
Values (1,N'One Plus',N'One Plus 5t',N'OnePlus 5T A5010 64GB Midnight Black, Dual Sim, 6.01", 6GB RAM, GSM Unlocked International Model, No Warranty', 3000.00,N'Black' );
go
INSERT INTO [dbo].[Product] (CategoryID,ProductBrand,ProductName,ProductDesc,ProductPrice,ProductColor)
Values (1,N'One Plus',N'One Plus 6',N'OnePlus 6 A6003 128GB Storage + 8GB Memory Factory Unlocked 6.28 inch AMOLED Display Android 8.1 - (Mirror Black) US Version with Warranty', 3500.00,N'Black' );
go
INSERT INTO [dbo].[Product] (CategoryID,ProductBrand,ProductName,ProductDesc,ProductPrice,ProductColor)
Values (2,N'Samsung',N'Samsung UE-40M5000SSXTK',N'Samsung UE-40M5000SSXTK 40" 102 Ekran Uydu Al�c�l� Full HD LED TV', 1500.00,N'Black' );
go
INSERT INTO [dbo].[Product] (CategoryID,ProductBrand,ProductName,ProductDesc,ProductPrice,ProductColor)
Values (2,N'Samsung',N'Samsung LT32E310MZ',N'Samsung LT32E310MZ 32" 82 Ekran Full HD LED Ekran', 1070.00,N'Black' );
go
---Images

Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (1,N'oneplusone',N'~/Content/Images/oneplusone.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (1,N'oneplus-one-1',N'~/Content/Images/oneplus-one-1.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (1,N'oneplusone-2',N'~/Content/Images/oneplusone-2.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (2,N'oneplusone',N'~/Content/Images/oneplusone.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (2,N'oneplus-one-1',N'~/Content/Images/oneplus-one-1.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (2,N'oneplusone-2',N'~/Content/Images/oneplusone-2.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (3,N'oneplus3',N'~/Content/Images/oneplus3.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (4,N'oneplus3',N'~/Content/Images/oneplus3.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (5,N'oneplus3',N'~/Content/Images/oneplus3.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (6,N'oneplus3t',N'~/Content/Images/oneplus3t.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (7,N'oneplus3t',N'~/Content/Images/oneplus3t.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (8,N'oneplus3t',N'~/Content/Images/oneplus3t.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (9,N'oneplus5',N'~/Content/Images/oneplus5.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (10,N'oneplus5',N'~/Content/Images/oneplus5.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (11,N'oneplus5',N'~/Content/Images/oneplus5.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (12,N'oneplus5',N'~/Content/Images/oneplus5.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (13,N'oneplus5t',N'~/Content/Images/oneplus5t.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (14,N'oneplus5t',N'~/Content/Images/oneplus5t.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (15,N'oneplus5t',N'~/Content/Images/oneplus5t.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (16,N'oneplus6',N'~/Content/Images/oneplus6.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (17,N'samsungTV1',N'~/Content/Images/samsungTV1.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (18,N'samsungTV2',N'~/Content/Images/samsungTV2.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (19,N'ipad',N'~/Content/Images/ipad.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (19,N'ipad1',N'~/Content/Images/ipad1.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (22,N'ipad1',N'~/Content/Images/samsungTV1.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (23,N'ipad1',N'~/Content/Images/samsungTV1.jpg');
go
Insert INTO [dbo].[Images] (ProductID,ImageCaption,ImagePath) 
Values (24,N'ipad1',N'~/Content/Images/samsungTV1.jpg');
go
---Cart
INSERT INTO [dbo].[Cart] (CartID,ProductID,[Count])
Values ('o.sanli',12,3);
go
INSERT INTO [dbo].[Cart] (CartID,ProductID,[Count])
Values ('i.cinar',4,2);
go

--Stored Procedures
create procedure removeItemFromCart(@CartID int)
as
begin

Declare @itemCount int;

Select @itemCount = c.[Count] from Cart as c Where RecordID = @CartID;

IF @itemCount > 1 
	BEGIN
		SET @itemCount = @itemCount-1
		UPDATE Cart SET [Count] = @itemCount WHERE RecordID = @CartID
	END
ELSE
	BEGIN
		Delete from Cart Where RecordID = @CartID
	END
end
go
create procedure EmptyCart(@CartID nvarchar(20))
as
begin
	Delete From Cart Where CartID = @CartID
end
go
--EXECUTE

--SELECTS
select * from [Category];
go
select * from [Images];
go
Select * from [Order];
go
select * from [dbo].[OrderDetail];
go
select* from [dbo].[Roles];
go
select * from [dbo].[Users];
go
select * from [dbo].[Address];
go
select * from [dbo].[Address] Where UserID = 2;
go
select * from [dbo].[Users];
go
select * from [dbo].[Cart];
go
select * from Product
go
Select * From [Address] where UserID = 3;
go
Select a.City as City, a.Country as Country, a.[State] as [State], a.PostalCode as PostalCode, a.AddressDesc as AddressDesc
from [Address] as a
Where a.UserID = 3;
go 
Select *
from Users as u left join [Address] as a
on u.UserID = a.UserID group by u.UserID,u.Username,u.Firstname,u.Lastname,u.[Password],u.Email,u.Phone,
u.CreatedAt,u.ModifiedAt,a.AddressID,a.UserID,a.City,a.Country,a.[State],a.PostalCode,a.AddressDesc
go
Select c.[Count] from Cart as c Where RecordID = 1;
go
Select * From Product as p  left join Category as c 
on p.CategoryID = c.CategoryID 
Where p.ProductName = null or c.CategoryID = 2;
go
--DROP
--drop procedure getUserByID;
--go
--drop procedure getUsers;
--go
--drop procedure getCart;
--go
--drop procedure removeItemFromCart;
--go
--drop procedure insertUser;
--go
--drop procedure getAddressByUserID;
--go
--DELETE
--delete from OrderDetail;
--go
--delete from [Order]
--go
--delete from Cart
--go